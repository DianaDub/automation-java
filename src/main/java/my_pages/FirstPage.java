package my_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.Urls;

public class FirstPage extends BasePage{
    public FirstPage(WebDriver driver) {
        super(driver);
    }
    /////////1111111//////////
    private final static class Locators {
        private static final By authorization = By.xpath("//i[@class='fa fa-user']");
        private static final By register = By.xpath("//a[@class='log-register']");
        private static final By registerName = By.id("name");
        private static final By registerPasswordField = By.id("password1");
        private static final By registerPasswordField2 = By.id("password2");
        private static final By registerMail = By.id("email");
        private static final By registerSendButton = By.xpath("//button[@name='submit']");
        private static final By registerFullname = By.id("fullname");
        private static final By myCabinetButton = By.xpath("//span[@class='show-login']//span");
        private static final By exitButton = By.xpath("(//ul[@class='login-menu']/li)[6]");
        /////22222222222/////
         private static final By loginName = By.id("login_name");
        private static final By loginPassword = By.id("login_password");
        private static final By enterButton = By.xpath("//button[@onclick='submit();']");
        private static final By errorMessage = By.xpath("//div[@class='berrors']/b");
        private static final By customerNameInMyCabinet = By.className("login-title");
        private static final By myProfile = By.xpath("(//ul[@class='login-menu']/li)[1]");
        private static final By editProfileButton = By.xpath("(//div[@class='usp__btn']/a)[2]");
        private static final By aboutYourselfField = By.xpath("(//div[@class='form__content']/textarea)[2]");
        private static final By updateButton = By.xpath("//button[@name='submit']");
        private static final By writeMessageButton = By.xpath("(//div[@class='usp__btn'])[1]");
        private static final By writeMessageField1 = By.id("pm_subj");
        private static final By writeMessageField2 = By.id("pm_text");
        private static final By sendMessageButton = By.xpath("(//button[@type='button'])[2]");
        private static final By successMessage = By.id("dlepopup");
        private static final By okButton = By.xpath("//button[@type='button']");
    }
    public FirstPage openMainPage() {
        driver.get(Urls.uaKino);
        return this;
    }
    public FirstPage authorizationOpen() {
        workWithElements.click(Locators.authorization);
        return this;
    }
    public FirstPage openRegisterPage() {
        workWithElements.click(Locators.register);
        return this;
    }
    public FirstPage insertName() {
        workWithElements.insertText(Locators.registerName, Strings.registerNameValue);
        return this;
    }
    public FirstPage insertPassword() {
        workWithElements.insertText(Locators.registerPasswordField, Strings.registerPasswordValue);
        return this;
    }
    public FirstPage insertPassword2() {
        workWithElements.insertText(Locators.registerPasswordField2, Strings.registerPasswordValue);
        return this;
    }
    public FirstPage insertMail() {
        workWithElements.insertText(Locators.registerMail, Strings.registerMailValue);
        return this;
    }
    public FirstPage sendRegisterForm() {
        workWithElements.click(Locators.registerSendButton);
        return this;
    }
    public FirstPage insertFullName() {
        workWithElements.insertText(Locators.registerFullname, Strings.registerFullnameValue);
        return this;
    }
    public FirstPage openCabinet() {
        workWithElements.click(Locators.myCabinetButton);
        return this;
    }
    public FirstPage exitFromCabinet() {
        workWithElements.click(Locators.exitButton);
        return this;
    }
    public FirstPage openMyProfile() {
        workWithElements.click(Locators.myProfile);
        return this;
    }

    public void closePage() {
        driver.close();
    }
    /////////2222222222//////////
    public FirstPage insertLoginName() {
        workWithElements.insertText(Locators.loginName, Strings.loginNameValue);
        return this;
    }
    public FirstPage insertLoginPassword() {
        workWithElements.insertText(Locators.loginPassword, Strings.loginPasswordValue);
        return this;
    }
    public FirstPage enterButtonClick() {
        workWithElements.click(Locators.enterButton);
        return this;
    }
    public FirstPage assertErrorMessage() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.errorMessage), Strings.errorAuthorizationMessage);
        return this;
    }
    /////////33333333333//////////
    public FirstPage insertWrightLoginName() {
        workWithElements.insertText(Locators.loginName, Strings.registerNameValue);
        return this;
    }
    public FirstPage insertWrightLoginPassword() {
        workWithElements.insertText(Locators.loginPassword, Strings.registerPasswordValue);
        return this;
    }
    public FirstPage assertCustomerName() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.customerNameInMyCabinet), Strings.registerNameValue);
        return this;
    }
    ///////////444444///////////
    public FirstPage openEditMyProfile() {
        workWithElements.click(Locators.editProfileButton);
        return this;
    }
    public FirstPage insertTextInFieldAboutYourself() {
        workWithElements.insertText(Locators.aboutYourselfField, Strings.textForFieldAboutYourself);
        return this;
    }
    public FirstPage tapUpdateButton() {
        workWithElements.click(Locators.updateButton);
        return this;
    }
    ///////////555555///////////
    public FirstPage tapWritingMessageButton() {
        workWithElements.click(Locators.writeMessageButton);
        return this;
    }
    public FirstPage insertMessageField1() {
        workWithElements.insertText(Locators.writeMessageField1, Strings.writeMessageText1);
        return this;
    }
    public FirstPage insertMessageField2() {
        workWithElements.insertText(Locators.writeMessageField2, Strings.writeMessageText2);
        return this;
    }
    public FirstPage sendMessageButton() {
        workWithElements.click(Locators.sendMessageButton);
        return this;
    }
    public FirstPage assertSuccessMessageAboutMessage() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.successMessage), Strings.successMessageAboutSendingMessage);
        return this;
    }
    public FirstPage tapOKButton() {
        workWithElements.click(Locators.okButton);
        return this;
    }
    private static class Strings {
        private static final String registerNameValue = "example1030";
        private static final String registerPasswordValue = "password1234567890";
        private static final String registerMailValue = "example1030@example.com";
        private static final String registerFullnameValue = "Example";
        private static final String loginNameValue = "example098";
        private static final String loginPasswordValue = "password";
        private static final String errorAuthorizationMessage = "Помилка авторизації";
        private static final String textForFieldAboutYourself = "I like movies.";
       private static final String writeMessageText1 = "Hello!";
       private static final String writeMessageText2 = "How are you?";
       private static final String successMessageAboutSendingMessage = "Ваше повідомлення успішно відправлене.";
    }

}
