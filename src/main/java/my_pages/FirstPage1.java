package my_pages;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class FirstPage1{

    /////////1111111//////////
    private final static class Locators {
        private static final By authorization = By.xpath("//i[@class='fa fa-user']");
        private static final By register = By.xpath("//a[@class='log-register']");
        private static final By registerName = By.id("name");
        private static final By registerPasswordField = By.id("password1");
        private static final By registerPasswordField2 = By.id("password2");
        private static final By registerMail = By.id("email");
        private static final By registerSendButton = By.xpath("//button[@name='submit']");
        private static final By registerFullname = By.id("fullname");
        private static final By registerSuccess = By.xpath("//div[@class='berrors']/b");
        private static final By myCabinetButton = By.xpath("//span[@class='show-login']//span");
        private static final By exitButton = By.xpath("(//ul[@class='login-menu']/li)[6]");
    }
    @Test
    public void checkPlusButton() throws InterruptedException {
        int countOfClick = 7;
        /*.openMainPage()
                .acceptCookies()
                .switchToFrame()
                .clickOnPlusButton(countOfClick)
                .checkResultNumber(countOfClick);
    */}
    /*@Given("set up driver")
    public void set_up_driver() {
        driver = DriverInit.startDriver();
        waiters = new CustomWaiters(driver);
        PageFactory.initElements(driver,this);
    }
    @Given("User is on page of the site")
    public void user_is_on_page_of_the_site() {
        driver.get(Urls.uaKino);
    }
    @When("Go to the page of registration")
    public void go_to_the_page_of_registration() {
        workWithElements.click(Locators.authorization);
        workWithElements.click(Locators.register);
    }
    @When("Insert valid data in the necessary fields and send form")
    public void insert_valid_data_in_the_necessary_fields_and_send_form() {
        workWithElements.insertText(Locators.registerName, Strings.registerNameValue);
        workWithElements.insertText(Locators.registerPasswordField, Strings.registerPasswordValue);
        workWithElements.insertText(Locators.registerPasswordField2, Strings.registerPasswordValue);
        workWithElements.insertText(Locators.registerMail, Strings.registerMailValue);
        workWithElements.click(Locators.registerSendButton);
    }
    @When("Insert full name of the customer and send form")
    public void insert_full_name_of_the_customer_and_send_form() {
        workWithElements.insertText(Locators.registerFullname, Strings.registerFullnameValue);
        workWithElements.click(Locators.registerSendButton);
        Assert.assertEquals(driver.findElement(Locators.registerSuccess).getText(), Strings.registerSuccessMessage);
    }
    @When("Exit from the page in tab My cabinet")
    public void exit_from_the_page_in_tab_my_cabinet() {
        workWithElements.click(Locators.myCabinetButton);
        workWithElements.click(Locators.exitButton);
    }
    @Then("The customer's page is closed")
    public void the_customer_s_page_is_closed() {
        driver.quit();
    }*/


    /*public FirstPage1 openMainPage() {
        driver.get(Urls.uaKino);
        return this;
    }
    public FirstPage1 authorizationOpen() {
        workWithElements.click(Locators.authorization);
        return this;
    }
    public FirstPage1 openRegisterPage() {
        workWithElements.click(Locators.register);
        return this;
    }
    public FirstPage1 insertName() {
        workWithElements.insertText(Locators.registerName, Strings.registerNameValue);
        return this;
    }
    public FirstPage1 insertPassword() {
        workWithElements.insertText(Locators.registerPasswordField, Strings.registerPasswordValue);
        return this;
    }
    public FirstPage1 insertPassword2() {
        workWithElements.insertText(Locators.registerPasswordField2, Strings.registerPasswordValue);
        return this;
    }
    public FirstPage1 insertMail() {
        workWithElements.insertText(Locators.registerMail, Strings.registerMailValue);
        return this;
    }
    public FirstPage1 sendRegisterForm() {
        workWithElements.click(Locators.registerSendButton);
        return this;
    }
    public FirstPage1 insertFullName() {
        workWithElements.insertText(Locators.registerFullname, Strings.registerFullnameValue);
        return this;
    }
    public FirstPage1 assertText() {
        Assert.assertEquals(driver.findElement(Locators.registerSuccess).getText(), Strings.registerSuccessMessage);
        return this;
    }
    public FirstPage1 openCabinet() {
        workWithElements.click(Locators.myCabinetButton);
        return this;
    }
    public FirstPage1 exitFromCabinet() {
        workWithElements.click(Locators.exitButton);
        return this;
    }
    public void closePage() {
        driver.quit();
        }
        */



    /////////2222222222//////////
    /*@FindBy(id = "login_name")
    private static WebElement loginField;
    @FindBy(id = "login_password")
    private static WebElement passwordField;
    @FindBy(xpath = "//button[@onclick='submit();']")
    private static WebElement authorizationButton;

*/
    /*private static class Strings {
        private static final String registerNameValue = "example1003";
        private static final String registerPasswordValue = "password1234567890";
        private static final String registerMailValue = "example123@example.com";
        private static final String registerFullnameValue = "Example";
        private static final String registerSuccessMessage = "Реєстрація успішно завершена";




        private static final String passwordValue = "1234567890";
        private static final String orderID = "1234567890";
        private static final String message = "My message for Contact Us Page";
        private static final String successMessageText = "Your message has been successfully sent to our team.";

    }*/

}
