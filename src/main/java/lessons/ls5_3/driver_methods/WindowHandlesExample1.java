package lessons.ls5_3.driver_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.Set;

public class WindowHandlesExample1 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.uHomki);
        //System.out.println(driver.getTitle());
        //System.out.println(driver.getWindowHandle());

        //open new window variant 1
        //показуємо, що будемо використовувати JavascriptExecutor і через драйвер запускати і відправляти якийсь скрипт
        Set<String> windowHandle1 = driver.getWindowHandles();
        ((JavascriptExecutor)driver).executeScript("window.open()");
        Set<String> windowHandle2 = driver.getWindowHandles();
        //видаляємо всі windowHandle1, щоб отримати дискриптор другого вікна
        windowHandle2.removeAll(windowHandle1);
        //забираємо ітератором потрібний елемент
        String descriptorOfSecondWindow = windowHandle2.iterator().next();
        //тепер переключаємось
        driver.switchTo().window(descriptorOfSecondWindow);
        //System.out.println(driver.getWindowHandles());
        driver.get(Urls.automationPractice);

        //open new window variant 2
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get(Urls.searchPage);

        //варіант відкриття нового вікна
        driver.switchTo().newWindow(WindowType.WINDOW);
        driver.get(Urls.automationPractice);
        Thread.sleep(3000);
        driver.close();
        Thread.sleep(2000);
        //спочатку переключитись на вікно інше із закритого, тоді можна переключатись
        driver.switchTo().window(descriptorOfSecondWindow);
        driver.get(Urls.uHomki);
        Thread.sleep(2000);
        driver.quit();
    }
}
