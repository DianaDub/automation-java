package lessons.ls5_3.driver_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RefreshExample {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.uHomki);
        WebElement searchField = driver.findElement(By.id("search_term"));
        searchField.sendKeys("Cat");
        WebElement searchButton = driver.findElement(By.cssSelector(".b-search__button"));
        searchButton.click();
        Thread.sleep(3000);
        //доповнює існуюче введене значення, щоб заново написати, потрібно очищати поле пошуку, де потрібно
        driver.findElement(By.id("search_term")).sendKeys("Cat");
        searchButton = driver.findElement(By.cssSelector(".b-search__button"));
        searchButton.click();
    }
}
