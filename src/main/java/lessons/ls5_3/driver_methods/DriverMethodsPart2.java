package lessons.ls5_3.driver_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.WebDriver;

public class DriverMethodsPart2 {
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        //all get methods
        //отримання назви сторінки
        System.out.println("Site1");
        driver.get(Urls.searchPage);
        System.out.println(driver.getTitle());
        //отримання посилання сторінки
        System.out.println(driver.getCurrentUrl());
        System.out.println("Site2");
        driver.get(Urls.automationPractice);
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        System.out.println("Site3");
        driver.get(Urls.uHomki);
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        //виводить всю інформацію сторінки
        //System.out.println(driver.getPageSource());
        driver.quit();
    }
}
