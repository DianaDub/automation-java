package lessons.ls5_3.driver_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.WebDriver;

public class DriverMethodsPart3 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        //navigate по кнопкам -> вперед, <- назад, оновити
        System.out.println("Site1");
        driver.get(Urls.searchPage);
        System.out.println(driver.getTitle());
        Thread.sleep(1500);
        driver.navigate().to(Urls.automationPractice);
        System.out.println("Site2");
        System.out.println(driver.getTitle());
        Thread.sleep(2000);
        driver.navigate().back();
        System.out.println("Site3");
        System.out.println(driver.getTitle());
        Thread.sleep(1000);
        driver.navigate().forward();
        System.out.println("Site4");
        System.out.println(driver.getTitle());
        driver.navigate().refresh();
        Thread.sleep(1000);
        driver.quit();
    }
}
