package lessons.ls5_3.webelement_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.*;

public class WebElementExampleClass3 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.accountPage);
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("ez-accept-all")).click();
        } catch (NoSuchElementException e) {
            System.out.println("No cookies button");
        }
        System.out.println("=======BEFORE CLICK=======");
        System.out.println("isDisplayed " + driver.findElement(By.id("RememberMe")).isDisplayed());
        System.out.println("isSelected " + driver.findElement(By.id("RememberMe")).isSelected());
        driver.findElement(By.id("RememberMe")).click();

        System.out.println("=======AFTER CLICK=======");
        System.out.println("isDisplayed " + driver.findElement(By.id("RememberMe")).isDisplayed());
        System.out.println("isSelected " + driver.findElement(By.id("RememberMe")).isSelected());

        driver.switchTo().newWindow(WindowType.TAB);

        //приклад з файлом html у викладача в репозиторії
        /*driver.get("some html file");
        Thread.sleep(5000);
        System.out.println("===BEFORE ENABLE===");
        System.out.println("isEnabled: " + driver.findElement(By.id("some id")).isEnabled());
        driver.findElement(By.id("some enable button")).click();

        System.out.println("===AFTER ENABLE===");
        System.out.println("isEnabled: " + driver.findElement(By.id("some id")).isEnabled())
        System.out.println(driver.findElement(By.id("some id")).getText());*/

        //WebElement element;
        //найти елемент всередині (відносно) елемента
        //element.findElement();
        driver.quit();
    }
}
