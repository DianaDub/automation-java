package lessons.ls5_3.webelement_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.*;

public class WebElementExampleClass2 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.accountPage);
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("ez-accept-all")).click();
        } catch (NoSuchElementException e) {
            System.out.println("No cookies button");
        }
        WebElement createAccountButton = driver.findElement(By.cssSelector(".btn-primary"));
        //getText
        System.out.println("Text: " + createAccountButton.getText());
        //get tagName
        System.out.println("TagName: " + createAccountButton.getTagName());
        //get CssValue
        System.out.println("Css value of background: " + createAccountButton.getCssValue("background"));
        //get attribute
        System.out.println("Attribute value: " + createAccountButton.getAttribute("type"));
        //get size, height, width
        System.out.println("Size of element: " + createAccountButton.getSize());
        System.out.println("Height of element: " + createAccountButton.getSize().height);
        System.out.println("Width of element: " + createAccountButton.getSize().width);
        Dimension dimension = new Dimension(20, 23);
        //get coordinates and location
        System.out.println("Coordinates of left top corner: " + createAccountButton.getLocation());
        System.out.println("X coordinates of left top corner: " + createAccountButton.getLocation().x);
        System.out.println("Y coordinates of left top corner: " + createAccountButton.getLocation().y);

        int centreX = createAccountButton.getLocation().x + createAccountButton.getSize().width/2;
        int centreY = createAccountButton.getLocation().y + createAccountButton.getSize().height/2;
        Point centreOfElement = new Point(centreX, centreY);
        System.out.println("Coordinates of centre: " + centreOfElement);
        driver.quit();
    }
}
