package lessons.ls5_3.webelement_methods;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebElementExampleClass1 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.uHomki);
        WebElement searchField = driver.findElement(By.id("search_term"));
        //sendKeys
        searchField.sendKeys("Cat");
        searchField.sendKeys(Keys.ENTER);
        Thread.sleep(4000);
        //clean enter's field
        driver.findElement(By.id("search_term")).clear();
        driver.findElement(By.id("search_term")).sendKeys("Cat");
        //click
        WebElement searchButton = driver.findElement(By.cssSelector(".b-search__button"));
        searchButton.click();
        Thread.sleep(2000);
        driver.quit();
    }
}
