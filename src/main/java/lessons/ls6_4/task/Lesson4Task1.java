package lessons.ls6_4.task;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.HashSet;
import java.util.Set;

public class Lesson4Task1 {
    public static String openLinkInNewTabAndReturnNewWindowHandle(WebDriver driver, String url) {
        Set<String> winHan1 = driver.getWindowHandles();
        ((JavascriptExecutor) driver).executeScript("window.open()");
        Set<String> winHan2 = driver.getWindowHandles();
        winHan2.removeAll(winHan1);
        String windowHandle2 = winHan2.iterator().next();
        driver.switchTo().window(windowHandle2);
        driver.get(url);
        return windowHandle2;
    }
    public static void openLinkInNewTab(WebDriver driver, Set<String> urls){
        for (String url: urls) {
            Set<String> windowHandles1 = driver.getWindowHandles();
            ((JavascriptExecutor)driver).executeScript("window.open()");
            //get new descriptor and turn on new page
            Set<String> windowHandles2 = driver.getWindowHandles();
            //in windowHandles2 delete all that windowHandles1 has
            windowHandles2.removeAll(windowHandles1);
            String windowHandle2 = windowHandles2.iterator().next();
            driver.switchTo().window(windowHandle2);
            driver.get(url);
        }
    }
    public static void printAllNamesOfTabs(WebDriver driver, Set<String> windowHandles){
        for (String handle: windowHandles) {
            driver.switchTo().window(handle);
            System.out.println(driver.getTitle());
        }
    }
    public static void printAllNamesOfTabs(WebDriver driver){
        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle: windowHandles) {
            driver.switchTo().window(handle);
            System.out.println(driver.getTitle());
        }
    }
    public static void main(String[] args) throws InterruptedException {
        //example1
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.rosetka);
        String windowHandle1 = driver.getWindowHandle();
        String windowHandle2 = openLinkInNewTabAndReturnNewWindowHandle(driver, Urls.automationPractice);
        String windowHandle3 = openLinkInNewTabAndReturnNewWindowHandle(driver, Urls.uHomki);

        Set<String> windowHandles = new HashSet<>();
        windowHandles.add(windowHandle1);
        windowHandles.add(windowHandle2);
        windowHandles.add(windowHandle3);
        printAllNamesOfTabs(driver, windowHandles);
        driver.quit();
        //example2
        driver = DriverInit.setUpDriver();
        driver.get(Urls.rosetka);
        //Set<String> urls = new HashSet<>(Arrays.asList(Urls.accountPage,Urls.automationPractice, Urls.uhomki));
        // ===
        Set<String> urls = new HashSet<>();
        urls.add(Urls.accountPage);
        urls.add(Urls.automationPractice);
        urls.add(Urls.uHomki);
        openLinkInNewTab(driver, urls);
        printAllNamesOfTabs(driver);
        //code below is written higher
        /*
        Set<String> windowHandles1 = driver.getWindowHandles();
        ((JavascriptExecutor)driver) .executeScript("window.open()");
        //get new descriptor and turn on new page
        Set<String> windowHandles2 = driver.getWindowHandles();
        //in windowHandles2 delete all that windowHandles1 has
        windowHandles2.removeAll(windowHandles1);
        String windowHandle2 = windowHandles2.iterator().next();
        driver.switchTo().window(windowHandle2);
        driver.get(Urls.automationPractice);
        */
        Thread.sleep(2000);
        driver.quit();
    }
}
//тобто, простіше створювати нові методи і користуватись ними, ніж щоразу прописувати одну і ту ж логіку
/*Відкрити в браузері три різні вкладки.
 На кожній вкладці відкрити окремо різні будь-які сторінки,
 вивести в консоль назви відкритих сторінок. Закрити браузер.*/