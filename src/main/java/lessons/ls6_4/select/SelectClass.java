package lessons.ls6_4.select;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SelectClass {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.htoKudi);
        Thread.sleep(2000);
        WebElement elementCountries = driver.findElement(By.id("itt_country"));
        Select countries = new Select(elementCountries);
        //select
        countries.selectByVisibleText("Катар");
        Thread.sleep(2000);
        countries.selectByValue("16");
        Thread.sleep(2000);
        countries.selectByIndex(5);
        Thread.sleep(2000);
        //isMultiple
        System.out.println();
        //перевіряємо чи може вибирати багато опцій одночасно - isMultiple
        System.out.println(countries.isMultiple());
        Select cities = new Select(driver.findElement(By.id("region_list")));
        System.out.println(cities.isMultiple());

        System.out.println("=====getAllOptions=====");
        //кожна опція це окремий веб-елемент
        List<WebElement> allOptions = cities.getOptions();
        for (WebElement element: allOptions) {
            System.out.println(element.getText() + " " + element.getAttribute("value"));
        }
        System.out.println("=====getAllSelectedOptions=====");
        cities.selectByVisibleText("Сапа");
        cities.selectByValue("11680");
        cities.selectByIndex(1);
        List<WebElement> selectedOptions = cities.getAllSelectedOptions();
        for (WebElement element: selectedOptions) {
            System.out.println(element.getText());
        }
        System.out.println("=====deselect=====");
        Thread.sleep(3000);
        cities.deselectByVisibleText("Сапа");
        Thread.sleep(2000);
        cities.deselectByValue("11680");
        cities.deselectAll();
        Thread.sleep(2000);
        for (WebElement element: cities.getAllSelectedOptions()) {
            System.out.println(element.getText());
        }
        countries.selectByIndex(3);
        Thread.sleep(2000);

        driver.quit();
    }
}
