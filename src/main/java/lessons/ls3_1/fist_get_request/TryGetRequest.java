package lessons.ls3_1.fist_get_request;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class TryGetRequest {
    public static void main(String[] args) {
        //get information of person with id 2
        RestAssured.baseURI = "https://reqres.in/";
        given()
                .when()
                .get("/api/users/2W")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
        Response response = RestAssured.get("/api/users/2");
        String responseString = response.getBody().asString();
        System.out.println(responseString);
    }
}
