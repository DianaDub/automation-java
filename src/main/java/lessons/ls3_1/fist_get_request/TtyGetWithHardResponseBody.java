package lessons.ls3_1.fist_get_request;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class TtyGetWithHardResponseBody {
    public static void main(String[] args) {
        RestAssured.baseURI = "https://reqres.in/";

        Response response = RestAssured.get("/api/users?page=2");
        //get body as a string
        String responseString = response.getBody().asString();
        System.out.println(responseString);
        //get json object from string
        JsonObject jsonObjectResponse = JsonParser.parseString(responseString).getAsJsonObject();
        //get value by key
        int total = jsonObjectResponse.get("total").getAsInt();
        System.out.println("Total amount of users is " + total);
        //get data
        JsonArray arrayOfUsers = jsonObjectResponse.getAsJsonArray("data").getAsJsonArray();
        System.out.println(arrayOfUsers.size());

        //with help for each
        //кожного юзера взяли як .jsonObject, так як у for не можна було взяти, тому взяли jsonElement
        for (JsonElement user: arrayOfUsers) {
            System.out.println(user.getAsJsonObject().get("id").getAsString());
            System.out.println(user.getAsJsonObject().get("email").getAsString());
            System.out.println(user.getAsJsonObject().get("first_name").getAsString());
        }
    }
}
