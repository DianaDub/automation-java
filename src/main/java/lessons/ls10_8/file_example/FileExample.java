package lessons.ls10_8.file_example;

import java.io.File;
import java.io.IOException;

public class FileExample {
    public static void main(String[] args) throws IOException {
        File fileTest = new File("C:\\Screenshots\\text.txt");
        fileTest.createNewFile();
        System.out.println("Name of file: " + fileTest.getName());
        System.out.println("Size of file: " +fileTest.length());
        System.out.println("Can i write in file? " + fileTest.canWrite());
        System.out.println("Can i read file? " + fileTest.canRead());
        System.out.println("Is it a file? " + fileTest.isFile());
        System.out.println("Is it a folder/directory? " + fileTest.isDirectory());

        File fileFolder = new File("C:\\Screenshots");
        System.out.println("Is it a file? " + fileFolder.isFile());
        System.out.println("Is it a folder/directory? " + fileFolder.isDirectory());
        int counter = 1;
        for (File file: fileFolder.listFiles()){
            System.out.println(file.getName());
            counter++;
        }
    }
}
