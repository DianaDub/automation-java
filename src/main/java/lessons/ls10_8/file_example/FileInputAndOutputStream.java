package lessons.ls10_8.file_example;

import java.io.*;

public class FileInputAndOutputStream {
    public static void main(String[] args) {
        //write in file
        try(FileOutputStream fileOutputStream = new FileOutputStream("C:\\Screenshots\\text.txt")
        ) {
            fileOutputStream.write("Text for example".getBytes());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        //read file
        byte[] bufferByte = new byte[10];
        StringBuilder textForRead = new StringBuilder();
        try(FileInputStream fileInputStream = new FileInputStream("C:\\Screenshots\\text.txt")
        ) {
            int count;
            while ((count=fileInputStream.read(bufferByte)) > 0) {
                for (int i = 0; i < count; i++){
                    textForRead.append((char) bufferByte[i]);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(textForRead);
    }
}
