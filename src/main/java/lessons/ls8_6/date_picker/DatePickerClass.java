package lessons.ls8_6.date_picker;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.time.LocalDate;

public class DatePickerClass {
    public static void chooseTomorrowsDate(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500)");
        driver.findElement(Locators.calendar).click();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        //System.out.println(tomorrow.getDayOfMonth());
        ///System.out.println(tomorrow.getMonth());

        int dayOfTomorrow = tomorrow.getDayOfMonth();
        String month = String.valueOf(tomorrow.getMonth());
        String monthCase = "";
        String monthCase2 = "";
        monthCase = monthCase + month.charAt(0);
        for (int i = 0; i < month.length(); i++) {
            if (i == 0) {
                continue;
            }
            monthCase2 = monthCase2 + month.charAt(i);
        }
        monthCase = monthCase + monthCase2.toLowerCase();
        //System.out.printf(monthCase);
        String xpath = "//div[contains(@aria-label, '" + monthCase + "')][text()='" + dayOfTomorrow + "']";
        driver.findElement(By.xpath(xpath)).click();
    }

    public static void chooseDateAndTime(WebDriver driver, int year, String month, int day, String time) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500)");
        driver.findElement(Locators.calendarWithTime).click();
        driver.findElement(Locators.year).click();
//        int i = 1;
//        for (WebElement element: driver.findElements(Locators.yearList)) {
//            System.out.println(i + ")" + element.getText());
//            i++;
//        }
        while (Integer.parseInt(driver.findElement(Locators.yearFirstButton).getText()) < year){
            driver.findElements(Locators.yearList).get(0).click();
        }
        while (Integer.parseInt(driver.findElement(Locators.yearLastButton).getText()) > year){
            driver.findElements(Locators.yearList).get(driver.findElements(Locators.yearList).size()-1).click();
        }
        driver.findElement(By.xpath("//div[@class='react-datepicker__year-option'][text()='"+year+"']")).click();
        driver.findElement(Locators.monthSelector).click();
        driver.findElement(By.xpath("//div[@class='react-datepicker__month-option'][text()='"+month+"']")).click();
        String xpath = "//div[contains(@aria-label, '" + month + "')][text()='"+ day +"']";
        driver.findElement(By.xpath(xpath)).click();
        driver.findElement(By.xpath("//li[@class='react-datepicker__time-list-item '][text()='"+time+"']")).click();
    }

    private static class Locators {
        private static final By calendar = By.id("datePickerMonthYearInput");
        private static final By calendarWithTime = By.id("dateAndTimePickerInput");
        private static final By year = By.cssSelector(".react-datepicker__year-read-view--selected-year");
        private static final By yearList = By.xpath("//div[@class='react-datepicker__year-option']");
        private static final By yearUpButton = By.xpath("//div[@class='react-datepicker__year-option'][1]");
        private static final By yearDownButton = By.xpath("//div[@class='react-datepicker__year-option'][12]");
        private static final By yearFirstButton = By.xpath("//div[@class='react-datepicker__year-option'][2]");
        private static final By yearLastButton = By.xpath("//div[@class='react-datepicker__year-option'][11]");
        private static final By monthSelector = By.cssSelector(".react-datepicker__month-read-view--selected-month");
    }

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.datePickerPage);
        //chooseTomorrowsDate(driver);
        chooseDateAndTime(driver, 2025, "May", 11, "21:00");
        Thread.sleep(5000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, -500)");
        chooseDateAndTime(driver, 2011, "June", 1, "09:00");
        Thread.sleep(5000);
        driver.quit();
    }
}
