package lessons.test_rail_integration;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class TestRailIntegration {
    public enum StatusForTest {
        PASSED(1), BLOCKED(2), RETEST(4), FAILED(5);

        StatusForTest(int statusValue) {
            this.statusValue = statusValue;
        }

        public int getStatusValue() {
            return statusValue;
        }
        public static int returnRandomStatus(){
            StatusForTest[] statusForTests = StatusForTest.values();
            return statusForTests[new Random().nextInt(statusForTests.length)].getStatusValue();
            //int randomValue = statusForTests[new Random().nextInt(statusForTests.length)].getStatusValue();
            //return randomValue;
        }

        //1 - passed, 2 - blocked, 3 - untested(can't use in code), 4 - retest, 5 - failed
        private int statusValue;

    }
    public static void main(String[] args) {
        //отримуємо екземпляр TestRail
        String testRailUrl = "https://didub2002.testrail.io/";
        String userName = "user@user.user";
        String password = "V2JWn45UDpv6mqA!";
        TestRail myTestRail = TestRail.builder(testRailUrl, userName, password)
                .applicationName("TestRailIntegration").build();

        //отримання списку всіх проєктів
        List<Project> projectsList = myTestRail.projects().list().execute();
        //отримання інформації про всі проєкти(назва та id)
        for (Project project: projectsList) {
            System.out.println("Name of the project is " + project.getName() + ". Id of the project is "
            + project.getId());
        }
        System.out.println("=====SUITES=====");
        //отримання списку suite-ів
        List<Suite> suiteList = myTestRail.suites().list(2).execute();
        for (Suite suite: suiteList) {
            System.out.println("Name of the suite is " + suite.getName() + ". Id of the suite is "
                    + suite.getId());
        }
        System.out.println("=====SECTIONS=====");
        //вивід секцій
        List<Section> sectionList = myTestRail.sections().list(1).execute();
        for (Section section: sectionList) {
            System.out.println("Name of the section is " + section.getName() + ". Id of the section is "
                    + section.getId());
        }
        System.out.println("=====CASES=====");
        //вивід кейсів
        List<CaseField> caseFieldsList = myTestRail.caseFields().list().execute();
        List<Case> caseList = myTestRail.cases().list(1, caseFieldsList).execute();
        for (Case testCase: caseList) {
            System.out.println("Name of the case is " + testCase.getTitle() + ". Id of the case is "
                    + testCase.getId());
        }
        //ID отримані попередньо потрібно буде використовувати для задання статусів тест-кейсам
        System.out.println("=====TEST RUN=====");
        //створення test run-у
        String time = String.format("%1$tH:%1$tM", new Date());
        Run run = myTestRail.runs().add(1, new Run().setName("Idea run " + time)).execute();
        //7
        //проставлення статусів для тест-кейсів
        //1 - passed, 2 - blocked, 3 - untested(can't use in code), 4 - retest, 5 - failed

        //зберемо всі id кейсів
        List<Integer> caseID = new ArrayList<>();
        for (Case testCase: caseList) {
            caseID.add(testCase.getId());
        }
        //у нашому кейсі будуть всі id
        List<ResultField> resultFieldList = myTestRail.resultFields().list().execute();
        for (Integer cases: caseID) {
            myTestRail.results().addForCase(run.getId(), cases, new Result().setStatusId(StatusForTest.returnRandomStatus()), resultFieldList).execute();
        }
        //отримання всіх тест ранів(показує і закриті і відкриті)

        List<Run> runList = myTestRail.runs().list(1).execute();
        for (Run testRun: runList) {
            System.out.println("Name of the testRun is " + testRun.getName() + ". Id of the testRun is "
                    + testRun.getId() + ". isCompleted: " + testRun.isCompleted());
            //чи закритий? //System.out.println(". isCompleted: " + testRun.isCompleted());
        }
        //закриття тест ранів
        myTestRail.runs().close(run.getId()).execute();
    }
}
