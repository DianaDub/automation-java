package lessons.ls4_2.locators;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class CssSelectorClass {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get("https://www.guinnessworldrecords.com/Account/Login?ReturnUrl=%2faccount");
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("ez-accept-all")).click();
        } catch (NoSuchElementException e) {
            System.out.println("No cookies button");
        }
        //By id css selector
        driver.findElement(By.cssSelector("#search")).click();
        //за назвою тега, атрибута та значення -> input[placeholder='Search']
        //за буквосполученням -> [placeholder*='earc']
        //по початковому словосполученню -> [placeholder^='Sea']
        //по кінцевому словосполученню -> [placeholder$='ch']
        driver.findElement(By.cssSelector("input[placeholder='Search']")).sendKeys("Tester");
        //приклад написання css selector -> "button[id=search-button].btn.search-form-btn#search-button"
        driver.findElement(By.cssSelector("button[id=search-button].btn.search-form-btn#search-button")).click();
        Thread.sleep(1000);
        System.out.println(driver.findElement(By.cssSelector("h2")).getText());
        //рух всередину вкладеності
        // .container section form .control-group .controls input#Username
        driver.quit();
    }
}
