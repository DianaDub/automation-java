package lessons.ls4_2.locators;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.TreeMap;

public class Xpath {
    public static void main(String[] args) throws InterruptedException {
        //повний xpath /html/body/div[1]/main/div/section/div/div[3]/a
        //відносний xpath //div[@class='columned block block-4-12']
        //можна шукати будь-який елемент по тексту
        //By.xpath with text
        WebDriver driver = DriverInit.setUpDriver();
        driver.get("https://www.guinnessworldrecords.com/search?term=%2A#");
        Thread.sleep(2000);
        ////button[text()='Search']
        ////input[@placeholder='Search']
        driver.findElement(By.xpath("//input[@placeholder='Search']")).sendKeys("Tallest man");
        driver.findElement(By.xpath("//*[text()='Search']")).click();
        Thread.sleep(3000);
        //By.xpath contains
        driver.get("https://www.guinnessworldrecords.com/records/apply-to-set-or-break-a-record/");
        //contains with text
        driver.findElement(By.xpath("//a[contains(text(), 'LICATION PROC')]")).click();
        //contains with attribute value
        List<WebElement> webElementList = driver.findElements(By.xpath("//a[contains(@href, 's/showcase')]"));
        System.out.println(webElementList.size());
        webElementList.get(10).click();
        Thread.sleep(5000);
        //contains and not contains
        // //div[contains(@class, 'block block-4-12')][not(contains(@class, 'columned'))]
        //if you can't find by one locator, you can write one else, but if it's not necessary, then don't do it
        // //*[@id='Username'][@autofocus='autofocus']
        //відносний шлях всередині відносного шляху
        driver.get("https://www.guinnessworldrecords.com/Account/Login?ReturnUrl=%2faccount");
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("ez-accept-all")).click();
        } catch (NoSuchElementException e) {
            System.out.println("No cookies button");
        }
        driver.findElement(By.xpath("(//div[@class='container'])[1]//*[@id='Username']")).sendKeys("AQA8");
        //xpath переміщення по DOM дереву
        /*
        /.. - вверх по тегу
        /імя_тега - вниз до вказаного тега
        перехід на батьківський тег - /parent::div
        перехід на дочірний тег - /child::a
        перехід на паралельний тег - /following - sibling:: *[1]
        предок - / ancestor::span
        нащадок - / descendant::h3
        */
        //якщо немає атрибутів, то можна такі відносні шляхи вказувати - //label[@for='Password']/../following-sibling::div/input
        driver.findElement(By.xpath("//label[@for='Password']/../following-sibling::div/input"))
                .sendKeys("PASS");
        //xpath-оператори >, <, >=, <=, !=
        driver.get("https://www.guinnessworldrecords.com/Account/Login?ReturnUrl=%2faccount");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@data-val-length-max!=100]")).sendKeys("email");
        driver.findElement(By.xpath("//input[@data-val-length-max<200]")).sendKeys("password");
    }
}
