package lessons.ls4_2.locators;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.TreeMap;

public class Locators {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get("https://www.guinnessworldrecords.com/records/apply-to-set-or-break-a-record/");
        Thread.sleep(1500);
        //By.id
        //побачити, які взагалі є локатори у By: By locator = By.
        /*By locator = By.id("search");
        WebElement searchButton = driver.findElement(locator);
        searchButton.click();*/
        //below is short variant
        driver.findElement(By.id("search")).click();
        //By.name
        driver.get("https://www.guinnessworldrecords.com/Account/Login?ReturnUrl=%2faccount");
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("ez-accept-all")).click();
        } catch (NoSuchElementException e) {
            System.out.println("No cookies button");
        }
        driver.findElement(By.name("Username")).sendKeys("Test");
        //якщо є кукіси чи можливі вони, потрібно використовувати try -> catch
        //By.tagName
        driver.findElement(By.id("search")).click();
        driver.findElement(By.id("search-term")).sendKeys("handstand push ups in one minute");
        driver.findElement(By.id("search-button")).click();
        Thread.sleep(3000);
        String textFromElement = driver.findElement(By.tagName("h2")).getText();
        System.out.println(textFromElement);
        //By.linkText (вказується повністю), partialLinkText (вказується частково)
        driver.get("https://www.guinnessworldrecords.com/records/apply-to-set-or-break-a-record/");
        Thread.sleep(3000);
        driver.findElement(By.linkText("APPLICATION PROCESS")).click();
        Thread.sleep(3000);
        System.out.println(driver.findElement(By.tagName("h1")).getText());
        driver.findElement(By.partialLinkText("RDS SHOWCAS")).click();
        Thread.sleep(3000);
        System.out.println(driver.findElement(By.tagName("h1")).getText());
        driver.quit();
    }
}
