package lessons.ls7_5.iframe;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IframeExampleV1 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.demoQAIframe);
        Thread.sleep(1500);
        //default content
        System.out.println(driver.findElement(By.tagName("h1")).getText());
        //switch to parent frame through id
        driver.switchTo().frame("frame1");
        System.out.println("===========");
        System.out.println(driver.findElement(By.tagName("body")).getText());
        //switch to child frame through web element
        WebElement iframe = driver.findElement(By.xpath("//iframe[contains(@srcdoc,'Child')]"));
        driver.switchTo().frame(iframe);
        System.out.println("===========");
        System.out.println(driver.findElement(By.xpath("//p[text()='Child Iframe']")).getText());
        //switch to back on parent frame
        driver.switchTo().parentFrame();
        System.out.println("=====Parent Frame=====");
        System.out.println(driver.findElement(By.tagName("body")).getText());
        //switch to child frame again
        driver.switchTo().frame(iframe);
        //and switch to default content, to first content
        driver.switchTo().defaultContent();
        System.out.println("=====Default Content=====");
        System.out.println(driver.findElement(By.tagName("h1")).getText());
        driver.quit();
    }
}
