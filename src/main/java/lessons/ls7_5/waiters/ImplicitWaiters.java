package lessons.ls7_5.waiters;

import io.github.bonigarcia.wdm.WebDriverManager;
import utils.Urls;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class ImplicitWaiters {
    public static void main(String[] args) {
        //неявні очікування - відпрацьовують по всьому коду
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        //очікування завантаження сторінки
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        //очікування відпрацювання javascript-ів
        driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(10));
        //очікування появи елементів
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get(Urls.datePickerPage);
        driver.quit();
    }
}
