package lessons.ls7_5.waiters;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ExplicitWaitersExampleThree {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.accountPage);

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        //elementToBeClickable
        //якщо є кукіси
        //wait.until(ExpectedConditions.elementToBeClickable(By.id("ez-accept-all"))).click();

        //presenceOfElementLocated
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search"))).click();
        Thread.sleep(3000);
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-term")));
        wait.until(ExpectedConditions.titleIs("Search Results | Guinness World Records"));
        wait.until(ExpectedConditions.titleContains("Results | Guinness World"));
        wait.until(ExpectedConditions.urlToBe("https://www.guinnessworldrecords.com/search?term=%2A"));
        driver.findElement(By.id("search-term")).sendKeys("Most handstand push ups male in one minute");
        driver.findElement(By.id("search-term")).sendKeys(Keys.ENTER);
        driver.quit();
    }
}
