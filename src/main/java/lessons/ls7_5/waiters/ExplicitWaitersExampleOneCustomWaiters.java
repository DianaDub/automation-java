package lessons.ls7_5.waiters;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import waiters.CustomWaiters;

public class ExplicitWaitersExampleOneCustomWaiters {
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.w3SchoolAlert);
        //кастомні(свої) очікування (нижче під кожним методом в коментарях приклад з просто wait)
        CustomWaiters waiters = new CustomWaiters(driver);

        //for cookies
        //visibilityOfElementLocated
        //waiters.waitForVisibility(By.id("accept-choices"));
        /*WebElement acceptCookies = wait.until(ExpectedConditions.visibilityOfElementLocated();By.id("accept-choices"));
        acceptCookies.click();*/

        //frameToBeAvailableAndSwitchToIt
        waiters.switchToFrame(By.id("iframeResult"));
        driver.findElement(By.xpath("//button[@onclick='myFunction()']")).click();
        /*wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("iframeResult")));
        driver.findElement(By.xpath("//button[@onclick='myFunction()']")).click();*/

        //alertIsPresent
        Alert alert = waiters.switchToAlert();
        System.out.println(alert.getText());
        alert.accept();
        /*Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        System.out.println(alert.getText());
        alert.accept();*/
        driver.quit();
    }
}
