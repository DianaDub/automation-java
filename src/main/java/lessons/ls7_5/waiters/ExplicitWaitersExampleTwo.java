package lessons.ls7_5.waiters;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ExplicitWaitersExampleTwo {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.accountPage);

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        //elementToBeClickable
        //якщо є кукіси
        //wait.until(ExpectedConditions.elementToBeClickable(By.id("ez-accept-all"))).click();

        //поки ми не клікнемо RememberMe(тобто, поки він не буде вибраний) програма далі не піде
        //elementToBeSelected
        wait.until(ExpectedConditions.elementToBeSelected(By.id("RememberMe")));
        driver.findElement(By.id("Password")).sendKeys("Password");

        //як тільки не буде відмічений, програма відразу піде далі
        //elementSelectionStateToBe
        wait.until(ExpectedConditions.elementSelectionStateToBe(By.id("RememberMe"), false));
        driver.findElement(By.id("Password")).clear();
        Thread.sleep(2000);

        //textToBePresentInElementValue
        wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("Username"), "AQA"));
        driver.get(Urls.rosetka);
        Thread.sleep(2000);

        //invisibilityOfElementLocated
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("Username")));
        driver.get(Urls.w3SchoolAlert);
        driver.quit();
    }
}
