package lessons.ls7_5.waiters;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

public class FluentWaitClass {
    public static void main(String[] args) {
        //вільні(гнучкі) очікування
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.w3SchoolAlert);
        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(10)) //наскільки часто будемо перевіряти, наприклад, кожну секунду
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(InvalidElementStateException.class)
                .ignoring(NoAlertPresentException.class)
                .ignoring(NoSuchFrameException.class);
        //це те ж саме, що прописано в CustomWaiters

        //після того, як налаштували цей елемент, використовуємо його як звичайний wait
        //приймаємо кукіси, якщо є
        //fluentWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accept-choices"))).click();
        fluentWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("iframeResult")));
        driver.findElement(By.xpath("//button[@onclick='myFunction()']")).click();
        Alert alert = fluentWait.until(ExpectedConditions.alertIsPresent());
        System.out.println(alert.getText());
        alert.accept();
        driver.quit();
    }
}
