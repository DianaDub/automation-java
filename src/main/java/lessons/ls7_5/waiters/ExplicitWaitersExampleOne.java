package lessons.ls7_5.waiters;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ExplicitWaitersExampleOne {
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.w3SchoolAlert);
        //явні очікування - відпрацьовують лише тоді, коли їх вказують
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //коли хочемо почекати, використувувати потрібно wait

        //for cookies
        //visibilityOfElementLocated
        //WebElement acceptCookies = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accept-choices")));
        //acceptCookies.click();

        //frameToBeAvailableAndSwitchToIt
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("iframeResult")));
        driver.findElement(By.xpath("//button[@onclick='myFunction()']")).click();

        //alertIsPresent
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        System.out.println(alert.getText());
        alert.accept();
        driver.quit();
    }
}
