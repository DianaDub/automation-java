package lessons.ls7_5.scroll;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Scroll {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.rosetka);
        Thread.sleep(2000);
        //клікаємо вниз 100 разів
        for (int i = 0; i < 100; i++) {
            new Actions(driver)
                    .sendKeys(Keys.ARROW_DOWN).perform();
        }

        Thread.sleep(3000);
        driver.findElement(By.name("search")).sendKeys("iphone");
        Thread.sleep(2000);
        //гортаємо вниз на 3
        for (int i = 0; i < 3; i++) {
            new Actions(driver)
                    .sendKeys(Keys.ARROW_DOWN).perform();
            Thread.sleep(2000);
        }
        new Actions(driver).sendKeys(Keys.ENTER).build().perform();
        Thread.sleep(3000);
        //гортаємо вниз на 50 пікселів
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500)");
        Thread.sleep(3000);

        driver.quit();
    }
}
