package lessons.ls7_5.date_picker;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.time.LocalDate;

public class DatePickerClass {
    public static void chooseTomorrowsDate(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500)");
        driver.findElement(Locators.calendar).click();
        LocalDate tomorrow = LocalDate.now().plusDays(30);
        //System.out.println(tomorrow.getDayOfMonth());
        ///System.out.println(tomorrow.getMonth());

        int dayOfTomorrow = tomorrow.getDayOfMonth();
        String month =String.valueOf(tomorrow.getMonth());
        String monthCase = "";
        String monthCase2 = "";
        monthCase = monthCase + month.charAt(0);
        for (int i = 0; i < month.length(); i++) {
            if (i == 0) {
                continue;
            }
            monthCase2 = monthCase2 + month.charAt(i);
        }
        monthCase = monthCase + monthCase2.toLowerCase();
        //System.out.printf(monthCase);
        String xpath = "//div[contains(@aria-label, '" + monthCase + "')][text()='"+ dayOfTomorrow +"']";
        driver.findElement(By.xpath(xpath)).click();
    }
    private static class Locators {
        private static final By calendar = By.id("datePickerMonthYearInput");

    }
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.datePickerPage);
        chooseTomorrowsDate(driver);
        driver.quit();
    }
}
