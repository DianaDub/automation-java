package lessons.ls7_5.double_context;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DoubleAndContextPageExampleClass {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.clickActionExPage);
        Thread.sleep(2000);
        /*якщо є кукіси
        try {
            driver.switchTo().frame("gdpr-consent-notice");
            driver.findElement(By.id("save")).click();
            driver.switchTo().defaultContent();
        } catch (NoSuchElementException e) {
            e.getMessage();
        }*/
        Actions actions = new Actions(driver);
        actions.contextClick(driver.findElement(By.cssSelector(".context-menu-one"))).perform();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//span[text()='Quit']")).click();
        Thread.sleep(300);
        Alert alert = driver.switchTo().alert();
        String textFromFrame = alert.getText();
        System.out.println(alert.getText());
        alert.accept();
        if (textFromFrame.contains("quit")){
            System.out.println("TEXT_1 PASSED");
        } else {
            System.out.println("TEXT_1 FAILED");
        }
        Thread.sleep(2000);
        actions.doubleClick(driver.findElement(By.xpath("//button[@ondblclick='myFunction()']"))).perform();
        alert = driver.switchTo().alert();
        textFromFrame = alert.getText();
        alert.accept();
        if (textFromFrame.equals("You double clicked me.. Thank You..")){
            System.out.println("TEXT_1 PASSED");
        } else {
            System.out.println("TEXT_1 FAILED");
        }
        Thread.sleep(2000);
        driver.quit();
    }
}
