package lessons.ls7_5.alert;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AlertExampleClass {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.alertExamplePage);
        By jsAlert = By.xpath("//button[@onclick='jsAlert()']");
        By jsConfirm = By.xpath("//button[@onclick='jsConfirm()']");
        By jsPrompt = By.xpath("//button[@onclick='jsPrompt()']");
        By result = By.id("result");
        String resultJsAlertAccept = "You successfully clicked an alert";
        String resultJsAlertDismiss = "You clicked: Cancel";
        String resultJsAlertSendKeys = "You entered: ";
        Thread.sleep(3000);

        driver.findElement(jsAlert).click();
        Thread.sleep(2000);
        System.out.println("=====JS Alert Test1=====");
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
        Thread.sleep(2000);
        boolean resultOfTest = driver.findElement(result).getText().equals(resultJsAlertAccept);
        if (resultOfTest){
            System.out.println("JS Alert Test1 PASSED");
        } else {
            System.out.println("JS Alert Test1 FAILED");
        }

        System.out.println("=====JS Confirm Test2=====");
        driver.findElement(jsConfirm).click();
        Thread.sleep(2000);
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.dismiss();
        Thread.sleep(2000);
        resultOfTest = driver.findElement(result).getText().equals(resultJsAlertDismiss);
        if (resultOfTest){
            System.out.println("JS Confirm Test1 PASSED");
        } else {
            System.out.println("JS Confirm Test1 FAILED");
        }

        System.out.println("=====JS Prompt Test3=====");
        driver.findElement(jsPrompt).click();
        Thread.sleep(2000);
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        String textToEnter = "Text for AQA";
        alert.sendKeys(textToEnter);
        alert.accept();
        Thread.sleep(2000);
        resultOfTest = driver.findElement(result).getText().equals(resultJsAlertSendKeys + textToEnter);
        if (resultOfTest){
            System.out.println("JS Prompt Test1 PASSED");
        } else {
            System.out.println("JS Prompt Test1 FAILED");
        }
        driver.quit();
    }
}
