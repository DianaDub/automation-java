package homeworks.hw3;

import driver_init.DriverInit;
import lessons.ls6_4.actions.ActionExampleClass2;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;


public class Task {
    public static class Urls {
        public static final String googleSearch = "https://www.google.com/search";
        public static final String guinnessWorldRecords = "https://www.guinnessworldrecords.com/account/register?";
        public static final String hyrTutorials = "https://www.hyrtutorials.com/p/alertsdemo.html";
        public static final String w3Schools = "https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_submit";
    }
    public static class Locators {
        public static final By googleSearchField = By.id("APjFqb");
        public static final By firstName = By.name("fname");
        public static final By lastName = By.name("lname");
        public static final By submitButton = By.xpath("//input[@value='Submit']");
        public static final By noteFromW3Schools = By.xpath("//div[@class='w3-panel w3-pale-yellow w3-leftbar w3-border-yellow']/p");
    }
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.googleSearch);
        driver.findElement(Locators.googleSearchField).sendKeys(Urls.guinnessWorldRecords);
        driver.findElement(Locators.googleSearchField).sendKeys(Keys.ENTER);
        String windowHandle1 = driver.getWindowHandle();
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get(Urls.guinnessWorldRecords);
        String windowHandle2 = driver.getWindowHandle();
        driver.switchTo().window(windowHandle1);
        driver.findElement(Locators.googleSearchField).clear();
        driver.findElement(Locators.googleSearchField).sendKeys(Urls.hyrTutorials);
        driver.findElement(Locators.googleSearchField).sendKeys(Keys.ENTER);
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get(Urls.hyrTutorials);
        String windowHandle3 = driver.getWindowHandle();
        driver.switchTo().window(windowHandle1);
        driver.get(Urls.w3Schools);
        driver.switchTo().frame("iframeResult");
        WebElement firstNameInput = driver.findElement(Locators.firstName);
        firstNameInput.clear();
        firstNameInput.sendKeys("Diana");
        WebElement secondNameInput = driver.findElement(Locators.lastName);
        secondNameInput.clear();
        secondNameInput.sendKeys("Dub");
        driver.findElement(Locators.submitButton).click();
        Thread.sleep(3000);
        WebElement submitText = driver.findElement(Locators.noteFromW3Schools);
        System.out.println("Message from site W3Schools: \n" + submitText.getText());
        driver.switchTo().window(windowHandle2);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,300)");
        driver.findElement(By.id("LastName")).sendKeys("Dub");
        driver.findElement(By.id("FirstName")).sendKeys("Diana");
        driver.findElement(By.id("DateOfBirthDay")).sendKeys("1");
        driver.findElement(By.id("DateOfBirthMonth")).sendKeys("6");
        driver.findElement(By.id("DateOfBirthYear")).sendKeys("2002");
        WebElement element = driver.findElement(By.id("Country"));
        Select region = new Select(element);
        region.selectByVisibleText("Ukraine");
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@id='State']")).sendKeys("Zhytomyr");
        driver.findElement(By.id("EmailAddress")).sendKeys("mailgmail@mails.cim");
        driver.findElement(By.id("ConfirmEmailAddress")).sendKeys("mailgmail@mails.cim");
        driver.findElement(By.id("Password")).sendKeys("mailgmail");
        driver.findElement(By.id("ConfirmPassword")).sendKeys("mailgmail2");
        driver.findElement(By.xpath("//label[@for='ConfirmPassword']")).click();
        Thread.sleep(1000);
        WebElement invalidPassword = driver.findElement(By.xpath("//span[@class='field-validation-error']"));
        System.out.println("Message from site GuinnessWorldRecords: \n" + invalidPassword.getText());
        driver.switchTo().window(windowHandle3);
        driver.navigate().refresh();
        js.executeScript("window.scrollBy(0,300)");
        driver.findElement(By.id("alertBox")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        System.out.println("Text from alert1: \n" + driver.findElement(By.xpath("//div[@id='output']")).getText());
        driver.findElement(By.id("confirmBox")).click();
        alert.dismiss();
        System.out.println("Text from alert2: \n" + driver.findElement(By.xpath("//div[@id='output']")).getText());
        driver.findElement(By.id("promptBox")).click();
        alert.sendKeys("'Final step of this task'");
        alert.accept();
        System.out.println("Text from alert2: \n" + driver.findElement(By.xpath("//div[@id='output']")).getText());
        driver.quit();
    }
}

/*Необхідно автоматизувати сценарій, показаний на відео “Сценарій для автоматизації.mp4”.
1) Для цього потрібно відкрити пошук гугл:
https://www.google.com/search
2) Після ввести в пошуковий рядок наступне посилання:
https://www.guinnessworldrecords.com/account/register?
Відкрити відповідне посилання в новому вікні, яке відображатиметься в результатах.
3) Після ввести в пошуковий рядок наступне посилання:
https://www.hyrtutorials.com/p/alertsdemo.html
Відкрити відповідне посилання в новому вікні, яке відображатиметься в результатах.
4) Відкрити в активному вікні наступне посилання:
https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_submit
У відкритому вікні заповнити поля своїм ім'ям та прізвищем та натиснути кнопку ‘Submit’.
Після виведення в консоль тексту даного елемента:
5) Перейти на вікно, в якому відкрито наступне посилання:
https://www.guinnessworldrecords.com/account/register?
У цьому вікні заповнити всі поля відповідною інформацією. У полях пароля та підтвердження
пароля ввести різні довільні паролі. Щоб з'явилося повідомлення про невідповідність даних паролів.
Вивести дане повідомлення в консоль.
6) Перейти на вікно, в якому відкрито наступне посилання:
https://www.hyrtutorials.com/p/alertsdemo.html
Натиснути почергово на наступні кнопки:
Після натискання кожної кнопки на екрані з'являться модальні вікна
Після натискання на першу кнопку натиснути “Ok” на модальному вікні та вивести у консоль
повідомлення у модулі “Popup box output”.
Після натискання на другу кнопку натиснути “Cancel” на модальному вікні та вивести в консоль
повідомлення в модулі “Popup box output”.
Після натискання на третю кнопку ввести текст “ Final step of this task” у модальному вікні та
натиснути “Ok”. Вивести в консоль повідомлення у модулі “Popup box output”.*/