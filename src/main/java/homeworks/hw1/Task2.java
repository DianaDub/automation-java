package homeworks.hw1;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Task2 {
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.accountPage);
        WebElement element1IconUser = driver.findElement(By.cssSelector(".icon-user"));
        WebElement element2Autofocus = driver.findElement(By.xpath("//input[@id='Username']"));
        informationAboutLocation(element1IconUser, element2Autofocus);
        driver.quit();
    }

    public static void informationAboutLocation(WebElement element1, WebElement element2) {
        Point location1 = element1.getLocation();
        Point location2 = element2.getLocation();

        if (location1.getY() < location2.getY()) {
            System.out.println("element1 located higher than element2.");
        } else if (location2.getY() < location1.getY()) {
            System.out.println("element2 located higher than element1.");
        } else {
            System.out.println("The both elements are at the same high.");
        }
        if (location1.getX() < location2.getX()) {
            System.out.println("element1 located left on the page.");
        } else if (location2.getX() < location1.getX()) {
            System.out.println("element2 located left on the page.");
        } else {
            System.out.println("The both elements are on the left.");
        }
        int volume1 = element1.getSize().getWidth() * element1.getSize().getHeight();
        int volume2 = element2.getSize().getWidth() * element2.getSize().getHeight();
        if (volume1 > volume2) {
            System.out.println("Volume of the element1 is big.");
        } else if (volume2 > volume1) {
            System.out.println("Volume of the element2 is big.");
        } else {
            System.out.println("These elements are equals.");
        }
    }
}
// Написати метод до параметрів якого приймаються два ВебЕлементи:
//метод виводить у консоль інформацію який із двох елементів розташовується вище на сторінці,
// який з елементів розташовується ліворуч на сторінці, а також який із елементів займає велику площу.
// Параметри методу можуть також включати інші аргументи, якщо це необхідно.