package homeworks.hw1;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Task4 {
    public static class Locators {
        private static final By SEARCH_FIELD = By.id("search_query_top");
        private static final By SEARCH_BUTTON = By.name("submit_search");
        private static final By PRINTED_CHIFFON_DRESS_LINK = By.xpath("(//a[@class='product-name'])[8]");
        private static final By SEARCH_SIZE = By.id("uniform-group_1");
        private static final By SIZE_M = By.xpath("//*[@value='2']");
        private static final By ADD_TO_CART = By.xpath("//button[@type='submit'][@class='exclusive']");
        private static final By CONTINUE_BUTTON = By.xpath("//span[@class='continue btn btn-default button exclusive-medium']");
        private static final By WOMEN = By.xpath("//a[@class='sf-with-ul'][1]");
        private static final By SHORT_SLEEVED_LINK = By.xpath("//a[@class='product-name'][@title='Blouse']");
        private static final By COLOR_8 = By.xpath("//a[@id='color_8']");
        private static final By CART_BUTTON = By.xpath("//a[@title='View my shopping cart']//b");
    }

    public static class Data {
        private static final String SEARCH_VALUE = "Printed Chiffon Dress";
        private static final String SEARCH_VALUE_2 = "Short Sleeved";
    }

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.automationPractice);
        Thread.sleep(2000);
        driver.findElement(Locators.SEARCH_FIELD).sendKeys(Data.SEARCH_VALUE);
        driver.findElement(Locators.SEARCH_BUTTON).click();
        Thread.sleep(2000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Thread.sleep(2000);
        driver.findElement(Locators.PRINTED_CHIFFON_DRESS_LINK).click();
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(2000);
        driver.findElement(Locators.SEARCH_SIZE);
        driver.findElement(Locators.SIZE_M).click();
        Thread.sleep(2000);
        driver.findElement(Locators.ADD_TO_CART).click();
        Thread.sleep(2000);
        driver.findElement(Locators.CONTINUE_BUTTON).click();
        Thread.sleep(2000);
        driver.findElement(Locators.WOMEN).click();
        Thread.sleep(2000);
        driver.findElement(Locators.SEARCH_FIELD).sendKeys(Data.SEARCH_VALUE_2);
        driver.findElement(Locators.SEARCH_BUTTON).click();
        Thread.sleep(2000);
        js.executeScript("window.scrollBy(0,500)");
        driver.findElement(Locators.SHORT_SLEEVED_LINK).click();
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(2000);
        driver.findElement(Locators.COLOR_8).click();
        driver.findElement(Locators.ADD_TO_CART).click();
        Thread.sleep(2000);
        driver.findElement(Locators.CONTINUE_BUTTON).click();
        Thread.sleep(2000);
        js.executeScript("window.scrollBy(0,-400)");
        driver.findElement(Locators.CART_BUTTON).click();
        Thread.sleep(3000);
        js.executeScript("window.scrollBy(0,500)");
        Thread.sleep(5000);
        driver.quit();
    }
}
//Написати програму, яка повторює дію на відео "HW_AUTOMATION_LESSON_3_UPDATE.mp4".
//http://www.automationpractice.pl/index.php