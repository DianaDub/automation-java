package homeworks.hw1;

import driver_init.DriverInit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.ArrayList;

public class Task1 {
    public static final String ZOO = "https://zoo.kiev.ua/";
    public static final String W3SCHOOLS = "https://www.w3schools.com/";
    public static final String TAXI = "https://taxi838.ua/ru/dnepr/";
    public static final String KLOPOTENKO = "https://klopotenko.com/";

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        ArrayList<String> urls = new ArrayList<>();
        urls.add(ZOO);
        urls.add(W3SCHOOLS);
        urls.add(TAXI);
        urls.add(KLOPOTENKO);
        for (String site : urls) {
            driver.switchTo().newWindow(WindowType.WINDOW);
            driver.get(site);
            System.out.println("Title is " + driver.getTitle() + " and link: " + driver.getCurrentUrl());
        }
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            if (driver.getTitle().toLowerCase().contains("зоопарк")) {
                driver.close();
            }
        }
        Thread.sleep(5000);
        driver.quit();
    }
}
//Написати програму, яка буде відкривати чотири різні сторінки у нових вікнах:
//https://zoo.kiev.ua/
//https://www.w3schools.com/
//https://taxi838.ua/ru/dnepr/
//https://klopotenko.com/
//Прописати цикл, який перемикатиметься по черзі через всі сторінки,
//для кожної сторінки виводити в консоль назву та посилання на цю сторінку.
//І закриватиме ту сторінку у назві якої є слово зоопарк.