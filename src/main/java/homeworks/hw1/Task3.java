package homeworks.hw1;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Task3 {
    public static void main(String[] args) {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(Urls.searchPage);
        WebElement element = driver.findElement(By.id("search-button"));
        informationAboutElement(element);
        driver.quit();
    }

    public static void informationAboutElement(WebElement element) {
        System.out.println("Meaning ID of element is '" + element.getAttribute("id") + "'.");
        System.out.println("Meaning of tag is '" + element.getTagName() + "'.");
        System.out.println("Meaning of class is '" + element.getAttribute("class") + "'.");
        System.out.println("Meaning of name is '" + element.getAttribute("name") + "'.");
        System.out.println("Text of the element is '" + element.getText() + "'.");
        int centreX = element.getLocation().x + element.getSize().width / 2;
        int centreY = element.getLocation().y + element.getSize().height / 2;
        Point centreOfElement = new Point(centreX, centreY);
        System.out.println("Coordinates of the element centre are " + centreOfElement + ".");
    }
}
//Написати метод який виводить повідомлення про ID елемента, значення тега елемента,
//значення класу елемента, значення атрибута name елемента, тексту даного елемента,
//а також координати центру контейнера даного елемента.