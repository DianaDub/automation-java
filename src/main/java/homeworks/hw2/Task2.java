package homeworks.hw2;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Task2 {
    public static final String DRAG_AND_DROP = "https://demo.guru99.com/test/drag_drop.html";

    public static class Locators {
        public static final By BUTTON_ORANGE_2 = By.xpath("//li[@id='fourth'][1]");
        public static final By AMT7 = By.id("amt7");
        public static final By BUTTON_ORANGE_4 = By.xpath("//li[@id='fourth'][2]");
        public static final By AMT8 = By.id("amt8");
        public static final By BUTTON_ORANGE_5 = By.xpath("//li[@id='credit2']");
        public static final By BANK = By.id("bank");
        public static final By BUTTON_ORANGE_6 = By.xpath("//li[@id='credit1']");
        public static final By LOAN = By.xpath("//ol[@id='loan']");
        public static final By BUTTON_GREEN = By.xpath("//a[@class='button button-green']");
    }

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(DRAG_AND_DROP);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Actions actions = new Actions(driver);
        actions.clickAndHold(driver.findElement(Locators.BUTTON_ORANGE_2))
                .moveToElement(driver.findElement(Locators.AMT7))
                .release().build().perform();
        actions.clickAndHold(driver.findElement(Locators.BUTTON_ORANGE_4))
                .moveToElement(driver.findElement(Locators.AMT8))
                .release().build().perform();
        actions.clickAndHold(driver.findElement(Locators.BUTTON_ORANGE_5))
                .moveToElement(driver.findElement(Locators.BANK))
                .release().build().perform();
        js.executeScript("window.scrollBy(0,500)");
        actions.clickAndHold(driver.findElement(Locators.BUTTON_ORANGE_6))
                .moveToElement(driver.findElement(Locators.LOAN))
                .release().build().perform();
        Thread.sleep(1000);
        actions.moveToElement(driver.findElement(Locators.BUTTON_GREEN));
        driver.findElement(Locators.BUTTON_GREEN).click();
        Thread.sleep(1000);
        driver.quit();
    }
}
/*2) https://demo.guru99.com/test/drag_drop.html
Написати програму, яка автоматизуватиме сценарій показаний на відео "Сценарій для автоматизії.mov".
Після виконання сценарію відео вивести в консоль теуст кнопки "Perfect!"*/