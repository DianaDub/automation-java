package homeworks.hw2;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Task1 {
    public static final String SITE = "http://only-testing-blog.blogspot.com/2014/01/textbox.html?";

    public static class Locators {
        public static final By SELECT_CARLIST = By.id("Carlist");
        public static final By SELECT_FROM_LB = By.xpath("//select[@name='FromLB']");
        public static final By LEFT_COLUMN = By.xpath("//input[@value='->']");
        public static final By RIGHT = By.name("ToLB");
        public static final By LEFT = By.name("FromLB");
    }

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get(SITE);
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,300)");
        Thread.sleep(2000);
        WebElement element1 = driver.findElement(Locators.SELECT_CARLIST);
        Select cars = new Select(element1);
        cars.selectByValue("Renault Car");
        Thread.sleep(3000);
        WebElement element2 = driver.findElement(Locators.SELECT_FROM_LB);
        Select counties = new Select(element2);
        counties.selectByVisibleText("France");
        counties.selectByVisibleText("Germany");
        counties.selectByVisibleText("Italy");
        counties.selectByVisibleText("Spain");
        driver.findElement(Locators.LEFT_COLUMN).click();
        System.out.println("Автомобілі доступні для вибору:");
        for (WebElement element : cars.getOptions()) {
            System.out.println(element.getText());
        }
        System.out.println("Країни з першої таблиці:");
        for (WebElement element : new Select(driver.findElement(Locators.LEFT)).getOptions()) {
            System.out.println(element.getText());
        }
        System.out.println("Країни з другої таблиці:");
        for (WebElement element : new Select(driver.findElement(Locators.RIGHT)).getOptions()) {
            System.out.println(element.getText());
        }
        driver.quit();
    }
}
/*1) http://only-testing-blog.blogspot.com/2014/01/textbox.html?
Написати програму, яка реалізує дію, показану на "видео1.mp4".
Після виконання програми на консолі має виводитись інформація в наступному вигляді:
Автомобілі доступні для вибору:
Volvo, BMW, Opel, Audi, Toyota, Renault, Maruti Car.
Країни з першої таблиці:
USA, ***, Japan, Mexico, India, Malaysia, Greece.
Країни з другої таблиці:
France, Germany, Italy, Spain.*/