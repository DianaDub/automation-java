package utils;

public class Urls {
    public static final String searchPage = "https://www.guinnessworldrecords.com/search?term=%2A#";
    public static final String applyRecordPage = "https://www.guinnessworldrecords.com/records/apply-to-set-or-break-a-record/";
    public static final String accountPage = "https://www.guinnessworldrecords.com/Account/Login?ReturnUrl=%2faccount";
    public static final String automationPractice = "http://www.automationpractice.pl/index.php";
    public static final String automationPracticeLoginPage = "http://www.automationpractice.pl/index.php?controller=authentication&back=my-account";
    public static final String automationPracticeContactUsPage = "http://www.automationpractice.pl/index.php?controller=contact";
    public static final String uHomki = "https://uhomki.prom.ua/ua/";
    public static final String rosetka = "https://rozetka.com.ua/";
    public static final String uaKino = "https://uakino.club/";
    public static final String htoKudi = "http://ktokuda.net/";
    public static final String danIT = "https://dan-it.com.ua/uk";
    public static final String dragNDropOne = "https://www.signesduquotidien.org/";
    public static final String demoQAIframe = "https://demoqa.com/nestedframes";
    public static final String w3SchoolIframe = "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_iframe_frameborder_css";
    public static final String alertExamplePage = "https://the-internet.herokuapp.com/javascript_alerts";
    public static final String clickActionExPage = "https://demo.guru99.com/test/simple_context_menu.html";
    public static final String datePickerPage = "https://demoqa.com/date-picker";
    public static final String w3SchoolAlert = "https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert";
    public static final String mixIDPage = "file:///C:/Users/Admin/Downloads/MixId.html";
    public static final String clickerPage = "https://www.clickspeedtester.com/click-counter/";
}
