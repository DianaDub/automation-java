package utils;

import driver_init.DriverInit2;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class MyAllureTestListeners implements ITestListener {
    /*public static void makeScreenshot(String name) {
        File screenshotFile = ((TakesScreenshot) DriverInit2.startDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshotFile, new File("./target/screenshots/" + name + ".png"));
        } catch (IOException E) {}
    }*/
    @Attachment(value = "Page Screenshot", type = "image/pgn")
    private byte[] saveScreenshot(byte[] screenshot) {
        return screenshot;
    }
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Test with name" + result.getMethod().getMethodName() + " started!");
    }
    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Test with name " + result.getMethod().getMethodName() + " passed!");
    }
    @Override
    public void onTestFailure(ITestResult result) {
        saveScreenshot(((TakesScreenshot) DriverInit2.startDriver()).getScreenshotAs(OutputType.BYTES));
    }
}
