package hw4.data_provider;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class DataProviderTest {
    static WebDriver driver;

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void close() {
        driver.quit();
    }

    @Test(dataProvider = "textToSend")
    public void findingWords(String textToSend) throws InterruptedException {
        driver.get("https://www.foxtrot.com.ua/");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys(textToSend);
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys(Keys.ENTER);
        Thread.sleep(3000);
        if (driver.findElement(By.xpath("//div[@class='page__title']/h1")).isDisplayed()) {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='page__title']/h1"))
                    .getText()
                    .contains(textToSend));
            if (driver.findElement(By.xpath("//div[@class='page__title']/h1")).getText()
                    .contains(textToSend)) {
                System.out.println("The result contains '" + textToSend + "'");
            } else {
                System.out.println("The result does not contain '" + textToSend + "'");
            }
        } else if (driver.findElement(By.xpath("//div[@class='search-page__box-title']"))
                .isDisplayed()) {
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='search-page__box-title']")).getText()
                    .contains(textToSend));
            if (driver.findElement(By.xpath("//div[@class='page__title']/h1")).getText()
                    .contains(textToSend)) {
                System.out.println("The result contains '" + textToSend + "'");
            } else {
                System.out.println("The result does not contain '" + textToSend + "'");
            }
        } else {
            System.out.println("ERROR");
        }
    }
    @DataProvider(name = "textToSend")
    public Object[][] createDataProvider(){
        return new Object[][]{
                {"машина"},
                {"input"},
                {"розуміння"}
        };
    }

}

//Написати тест, який перевірятиме пошуковий рядок сайту https://www.foxtrot.com.ua/
//Використовувати як перевірочні слова три наступні слова: “машина”, “input”, “розуміння”.
//• Якщо після пошуку слова, що вводиться, користувач бачить сторінку такого типу:
//То у вашому асерті необхідно переконатися, що виділений елемент містить потрібне слово.
//• Якщо після пошуку слова, що вводиться, користувач бачить сторінку такого типу:
//То у вашому асерті також необхідно переконатися, що виділений елемент містить потрібне слово.
// Вирішити це завдання використовуючи @DataProvider.