package hw4.priority;

import org.testng.annotations.Test;

import static org.junit.Assert.assertTrue;

public class Priority2 {
    @Test (dependsOnMethods = {"b"})
    public void a() {
        assertTrue(true);
    }
    @Test(dependsOnMethods = {"c"})
    public void b() {
        assertTrue(true);
    }
    @Test(dependsOnMethods = {"d"})
    public void c() {
        assertTrue(true);
    }
    @Test(dependsOnMethods = {"e"})
    public void d() {
        assertTrue(true);
    }
    @Test(dependsOnMethods = {"f"})
    public void e() {
        assertTrue(true);
    }
    @Test(dependsOnMethods = {"g"})
    public void f() {
        assertTrue(true);
    }
    @Test
    public void g() {
        assertTrue(true);
    }
}
/*1. Створити окремий пакет priority. У ньому клас Priority1.
У цьому класі створити тести a, b, c, d, e, f, g такого плану:
@Test
public void a() {
        assertTrue(true);
    }
Зробити так, щоб при запуску даного класу ці тести проходили у порядку зворотному алфавітному.
Вигадати принаймні два способи, як це можна зробити.
*/