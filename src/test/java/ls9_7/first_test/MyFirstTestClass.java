package ls9_7.first_test;

import driver_init.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MyFirstTestClass {
    @Test
    public void myFirstTest() throws InterruptedException {
        WebDriver driver = DriverInit.setUpDriver();
        driver.get("https://www.guinnessworldrecords.com/search?term=%2A");
        Thread.sleep(2000);
        String textToSend = "Handstand";
        driver.findElement(By.id("search-term")).sendKeys(textToSend);
        driver.findElement(By.id("search-term")).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.id("search-term")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.tagName("h2")).getText().contains(textToSend),
                "In element with text " + driver.findElement(By.id("search-term")).getText()
        + " there is no " + textToSend);

        Assert.assertFalse(driver.findElement(By.tagName("h2")).getText().contains("airplane"));
        System.out.println(driver.getTitle());
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
        Assert.assertNotEquals(driver.getTitle(), "Search Results | Guinness World Records2");

        driver.quit();
    }
}
