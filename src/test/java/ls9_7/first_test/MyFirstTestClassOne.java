package ls9_7.first_test;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import waiters.CustomWaiters;

public class MyFirstTestClassOne {
    static WebDriver driver;
    static CustomWaiters customWaiters;
    @BeforeClass
    public void setUp(){
        driver = DriverInit.setUpDriver();
        customWaiters = new CustomWaiters(driver);
    }
    @AfterClass
    public void stopDriver() {
        driver.quit();
    }
    @Test
    public void myFirstTestOne(){
        driver.get(Urls.searchPage);
        String textToSend = "Handstand";
        customWaiters.waitForVisibility(By.id("search-term")).sendKeys(textToSend);
        driver.findElement(By.id("search-term")).sendKeys(Keys.ENTER);
        customWaiters.waitForVisibility(By.tagName("h2"));
        Assert.assertTrue(driver.findElement(By.id("search-term")).isDisplayed());
        /*Assert.assertTrue(driver.findElement(By.id("search-term")).getText().contains(textToSend),
                "In element with text '" + driver.findElement(By.id("search-term")).getText() +
                "', there is no text '" + textToSend + "'");*/
        Assert.assertTrue(driver.findElement(By.tagName("h2")).getText().contains(textToSend),
                "In element with text '" + driver.findElement(By.tagName("h2")).getText() +
                        "', there is no text '" + textToSend + "'");
        Assert.assertFalse(driver.findElement(By.tagName("h2")).getText().contains("airplane"));
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
        Assert.assertNotEquals(driver.getTitle(), "Search Results | Guinness World Records21");
    }
    @Test
    public void myFirstTestOne2(){
        driver.get(Urls.searchPage);
        String textToSend = "Handstand";
        customWaiters.waitForVisibility(By.id("search-term")).sendKeys(textToSend);
        driver.findElement(By.id("search-term")).sendKeys(Keys.ENTER);
        customWaiters.waitForVisibility(By.tagName("h2"));
        Assert.assertTrue(driver.findElement(By.id("search-term")).isDisplayed());
        /*Assert.assertTrue(driver.findElement(By.id("search-term")).getText().contains(textToSend),
                "In element with text '" + driver.findElement(By.id("search-term")).getText() +
                "', there is no text '" + textToSend + "'");*/
        Assert.assertTrue(driver.findElement(By.tagName("h2")).getText().contains(textToSend),
                "In element with text '" + driver.findElement(By.tagName("h2")).getText() +
                        "', there is no text '" + textToSend + "'");
        Assert.assertFalse(driver.findElement(By.tagName("h2")).getText().contains("airplane"));
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
        Assert.assertNotEquals(driver.getTitle(), "Search Results | Guinness World Records21");
    }
    @Test
    public void myFirstTestOne3(){
        Assert.assertTrue(false);
    }
}
