package ls9_7.first_test;

import org.testng.annotations.*;

public class AnnotationOrderExample {
    @BeforeSuite
    public void beforeSuite() {
        System.out.println("=====BEFORE SUITE=====");
    }
    @BeforeTest
    public void beforeTest() {
        System.out.println("=====BEFORE TEST=====");
    }
    @BeforeClass
    public void beforeClass() {
        System.out.println("=====BEFORE CLASS=====");
    }
    @BeforeMethod
    public void beforeMethod() {
        System.out.println("=====BEFORE METHOD=====");
    }
    @Test
    public void test() {
        System.out.println("=====TEST=====");
    }
    @AfterMethod
    public void afterMethod() {
        System.out.println("=====BEFORE METHOD=====");
    }
    @AfterClass
    public void afterClass() {
        System.out.println("=====AFTER CLASS=====");
    }
    @AfterTest
    public void afterTest() {
        System.out.println("=====AFTER TEST=====");
    }
    @AfterSuite
    public void afterSuite() {
        System.out.println("=====AFTER SUITE=====");
    }
}
