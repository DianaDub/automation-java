package ls9_7.data_provider;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.Urls;
import waiters.CustomWaiters;

public class DataProviderTest {
    static WebDriver driver;
    static CustomWaiters customWaiters;

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        customWaiters = new CustomWaiters(driver);
    }
    @AfterClass
    public void close() {
        driver.quit();
    }

    @Test(dataProvider = "loginPassAndMailValue")
    public void negativeLoginTest(String mail, int password) {
        driver.get(Urls.automationPracticeLoginPage);
        customWaiters.waitForVisibility(By.id("passwd"));
        driver.findElement(By.id("email")).sendKeys("senkey@keys.keys");
        driver.findElement(By.id("passwd")).sendKeys(String.valueOf(password));
        driver.findElement(By.id("SubmitLogin")).click();
        customWaiters.waitForVisibility(By.cssSelector(".alert.alert-danger p"));
        Assert.assertEquals(driver.findElement(By.cssSelector(".alert.alert-danger p")).getText(),
                "There is 1 error");
    }
    @DataProvider(name = "loginPassAndMailValue")
    public Object[][] createData(){
        return new Object[][] {
                {"invalid mail1", 1314342},
                {"invalid mail2", 4679980},
                {"invalid mail3", 34245},
                {"invalid mail4", 54365},
                {"invalid mail5", 436324323}
        };
    }

}
