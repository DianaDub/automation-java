package ls9_7.grouping;

import driver_init.DriverInit;
import utils.Urls;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GroupsTest {
    static WebDriver driver;
    @BeforeClass(groups = {"smoke", "regression"})
    public void setUp(){
        driver = DriverInit.setUpDriver();
    }
    @AfterClass(groups = {"smoke", "regression"})
    public void stop(){
        driver.quit();
    }
    @Test(groups = {"regression"})
    public void a6(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"regression", "smoke"})
    public void a(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"smoke"})
    public void a4(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
        Assert.assertTrue(false);
    }
    @Test(groups = {"regression"})
    public void a2(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"smoke"})
    public void a1(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"regression"})
    public void a3(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"regression"})
    public void a5(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"smoke"})
    public void a7(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
    @Test(groups = {"smoke"})
    public void a8(){
        driver.get(Urls.searchPage);
        Assert.assertEquals(driver.getTitle(), "Search Results | Guinness World Records");
    }
}
