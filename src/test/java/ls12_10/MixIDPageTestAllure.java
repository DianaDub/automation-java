package ls12_10;

import driver_init.DriverInit2;
import io.qameta.allure.*;
import lessons.ls10_8.mix_id.MixIDPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.MyAllureTestListeners;

@Listeners({MyAllureTestListeners.class})
public class MixIDPageTestAllure {
    private WebDriver driver;
    private MixIDPage mixIDPage;

    @BeforeClass
    public void setUp() {
        driver = DriverInit2.startDriver();
        mixIDPage = new MixIDPage(driver);
    }

    @AfterClass
    public void stop() {
        driver.quit();
    }

    @Test
    @Description("This is check how elements will be dragged to the target area...")
    @Feature("NORMAL severity test")
    @TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void dragToAreaTest_C106() {
        mixIDPage.openMainPage()
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.STAR)
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.BALOON)
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.ROCKET)
                .assertDragAndDropElements();
    }
    @Description("This is check how select is working...")
    @Feature("BLOCKER severity test")
    @TmsLink("https://didub2002.testrail.io/index.php?/tests/view/105&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.BLOCKER)
    @Test
    public void selectTest_C107() {
            mixIDPage.openMainPage()
                    .selectOption(MixIDPage.Options.TWO)
                    .assertSelectedOption(MixIDPage.Options.TWO);
    }
    @Test
    public void clickCounterButtonCheck_C109(){
        int amountOfClicking = 21;
        mixIDPage.openMainPage()
                .clickOnCounterButtonNTimes(amountOfClicking)
                .assertValueAfterClickCounterButton(22);
    }
    /*@Test
    public void doubleClickButtonTest_C108(){
        mixIDPage.openMainPage()
                .clickDoubleClickButton()
                .assertDoubleClickAlert();
    }

    @Test
    public void checkAnimalGeneration_C110(){
        String animal = "Медведь";
        mixIDPage.openMainPage()
                .clickUntilAnimalWillBeFound(animal)
                .assertAnimalResultField(animal);
    }*/
}
