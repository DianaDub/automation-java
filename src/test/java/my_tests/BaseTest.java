package my_tests;

import driver_init.DriverInit;
import driver_init.DriverInit11;
import functions.CustomWaiters;
import functions.WorkWithElements;
import my_pages.BasePage;
import my_pages.FirstPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.MyAllureTestListeners;

@Listeners({MyAllureTestListeners.class})
public class BaseTest {
    protected WebDriver driver = DriverInit11.startDriver();;
    protected CustomWaiters waiters = new CustomWaiters(driver);
    protected WorkWithElements workWithElements = new WorkWithElements(driver, waiters);
    protected BasePage basePage = new BasePage(driver);
    protected FirstPage firstPage = new FirstPage(driver);

    @BeforeSuite
    public void init(){
        driver = DriverInit11.startDriver();
    }
    @AfterSuite
    public void stop() {
        driver.quit();
    }
}
