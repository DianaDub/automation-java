package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-report/cucumber.html",
                "json:target/cucumber-report/cucumber.json"},
        features = {"src/test/resources/features2"},
        glue = {"step_definitions"}
)
//запускає усі фіча файли з папки 2(features) і кроки шукає для них у step_definitions
public class TestRunner extends AbstractTestNGCucumberTests {
}
