package ls11_9;

import driver_init.DriverInit2;
import lessons.ls10_8.mix_id.MixIDPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners
public class TestRailIntegrationTest {
    private WebDriver driver;
    private MixIDPage mixIDPage;

    @BeforeClass
    public void setUp() {
        driver = DriverInit2.startDriver();
        mixIDPage = new MixIDPage(driver);
    }

    @AfterClass
    public void stop() {
        driver.quit();
    }

    @Test
    public void dragToAreaTest() {
        mixIDPage.openMainPage()
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.STAR)
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.BALOON)
                .dragElementToArea2(MixIDPage.ElementsForDragNDrop.ROCKET)
                .assertDragAndDropElements();
    }
    @Test
    public void selectTest() {
            mixIDPage.openMainPage()
                    .selectOption(MixIDPage.Options.TWO)
                    .assertSelectedOption(MixIDPage.Options.TWO);
    }
    @Test
    public void doubleClickButtonTest(){
        mixIDPage.openMainPage()
                .clickDoubleClickButton()
                .assertDoubleClickAlert();
    }
    @Test
    public void clickCounterButtonCheck(){
        int amountOfClicking = 21;
        mixIDPage.openMainPage()
                .clickOnCounterButtonNTimes(amountOfClicking)
                .assertValueAfterClickCounterButton(amountOfClicking);
    }
    @Test
    public void checkAnimalGeneration(){
        String animal = "Медведь";
        mixIDPage.openMainPage()
                .clickUntilAnimalWillBeFound(animal)
                .assertAnimalResultField(animal);
    }
}
